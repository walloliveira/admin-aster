// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Buefy from 'buefy';
import VueI18n from 'vue-i18n';
import App from './App';
import router from './router';
import i18nConfig from './assets/i18n-config.json';

Vue.config.productionTip = false;
Vue.use(Buefy);
Vue.use(VueI18n);
const i18n = new VueI18n(i18nConfig);
/* eslint-disable no-new */
Vue.prototype.$locale = {
  change(language) {
    localStorage.setItem('lang', language);
    i18n.locale = language;
  },
  current() {
    return localStorage.getItem('lang');
  },
};
new Vue({
  el: '#app',
  router,
  i18n,
  components: { App },
  template: '<App/>',
});
