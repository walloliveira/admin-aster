import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/pages/Home';
import Login from '@/pages/Login';
import ScheduleList from '@/pages/Schedule/ScheduleList';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        breadcrumbLabel: 'Home',
      },
      children: [
        {
          path: '/schedules',
          name: 'ScheduleList',
          component: ScheduleList,
          meta: {
            breadcrumbLabel: 'Schedules',
          },
        },
      ],
    },
  ],
});
